#!/bin/bash

export PATH=/home/builder/depot_tools:$PATH
cd /home/builder/WebRTC-build/files
if [ ! -d "build" ]; then
	mkdir build
fi
cd build
cmake ..
make
sudo make install